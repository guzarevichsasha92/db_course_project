-- Enable pgcrypto extension for password hashing
CREATE EXTENSION IF NOT EXISTS pgcrypto;

-- Function to backup data to CSV files and then load new data from CSV files into Users and Universities tables
CREATE OR REPLACE FUNCTION backup_and_load_data_from_csv(
  users_file_path TEXT,
  universities_file_path TEXT,
  backup_folder_path TEXT
)
RETURNS VOID AS $$
DECLARE
  backup_universities_file TEXT := backup_folder_path || '/backup_universities.csv';
  backup_users_file TEXT := backup_folder_path || '/backup_users.csv';
BEGIN
  -- Create backup of current data in CSV format
  BEGIN
    EXECUTE format('COPY (SELECT * FROM Universities) TO %L WITH (FORMAT CSV, HEADER, DELIMITER '','', QUOTE ''"'', ENCODING ''UTF8'')', backup_universities_file);
  EXCEPTION
    WHEN others THEN
      RAISE EXCEPTION 'Error backing up data from Universities: %', SQLERRM;
  END;

  BEGIN
    EXECUTE format('COPY (SELECT * FROM Users) TO %L WITH (FORMAT CSV, HEADER, DELIMITER '','', QUOTE ''"'', ENCODING ''UTF8'')', backup_users_file);
  EXCEPTION
    WHEN others THEN
      RAISE EXCEPTION 'Error backing up data from Users: %', SQLERRM;
  END;

  -- Drop temporary tables if they exist
  DROP TABLE IF EXISTS TempUniversities;
  DROP TABLE IF EXISTS TempUsers;

  -- Create temporary tables for data import
  CREATE TEMP TABLE TempUniversities (
    university_name VARCHAR(100),
    address TEXT,
    city VARCHAR(50),
    country VARCHAR(50)
  ) ON COMMIT DROP;

  CREATE TEMP TABLE TempUsers (
    user_name VARCHAR(50),
    user_surname VARCHAR(50),
    sex VARCHAR(10),
    password TEXT,
    email VARCHAR(100),
    university_name VARCHAR(100)
  ) ON COMMIT DROP;

  -- Load new data into temporary tables
  BEGIN
    EXECUTE format('COPY TempUniversities FROM %L WITH (FORMAT CSV, HEADER, DELIMITER '','', QUOTE ''"'', ENCODING ''UTF8'')', universities_file_path);
  EXCEPTION
    WHEN others THEN
      RAISE EXCEPTION 'Error loading data into TempUniversities: %', SQLERRM;
  END;

  BEGIN
    EXECUTE format('COPY TempUsers FROM %L WITH (FORMAT CSV, HEADER, DELIMITER '','', QUOTE ''"'', ENCODING ''UTF8'')', users_file_path);
  EXCEPTION
    WHEN others THEN
      RAISE EXCEPTION 'Error loading data into TempUsers: %', SQLERRM;
  END;

  -- Data cleaning: Remove unnecessary characters, spaces, and quotes
  UPDATE TempUniversities
  SET university_name = trim(both '"' from btrim(university_name)),
    address = trim(both '"' from btrim(address)),
    city = trim(both '"' from btrim(city)),
    country = trim(both '"' from btrim(country));

  UPDATE TempUsers
  SET user_name = trim(both '"' from btrim(user_name)),
    user_surname = trim(both '"' from btrim(user_surname)),
    sex = trim(both '"' from btrim(sex)),
    password = trim(both '"' from btrim(password)),
    email = trim(both '"' from btrim(email)),
    university_name = trim(both '"' from btrim(university_name));

  -- More detailed data cleaning steps (Example)
  -- Remove special characters
  UPDATE TempUniversities
  SET university_name = regexp_replace(university_name, '[^a-zA-Z0-9 ]', '', 'g'),
    address = regexp_replace(address, '[^a-zA-Z0-9 ]', '', 'g'),
    city = regexp_replace(city, '[^a-zA-Z0-9 ]', '', 'g'),
    country = regexp_replace(country, '[^a-zA-Z0-9 ]', '', 'g');

  UPDATE TempUsers
  SET user_name = regexp_replace(user_name, '[^a-zA-Z0-9 ]', '', 'g'),
    user_surname = regexp_replace(user_surname, '[^a-zA-Z0-9 ]', '', 'g'),
    sex = regexp_replace(sex, '[^a-zA-Z0-9 ]', '', 'g'),
    password = regexp_replace(password, '[^a-zA-Z0-9 ]', '', 'g'),
    university_name = regexp_replace(university_name, '[^a-zA-Z0-9 ]', '', 'g');

  -- Enhanced email validation using regex pattern
  DELETE FROM TempUsers
  WHERE email !~* '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$';

  -- Remove rows with empty required fields in TempUniversities
  DELETE FROM TempUniversities
  WHERE COALESCE(university_name, '') = ''
    OR COALESCE(city, '') = ''
    OR COALESCE(country, '') = '';

  -- Remove rows with empty required fields in TempUsers
  DELETE FROM TempUsers
  WHERE COALESCE(user_name, '') = ''
    OR COALESCE(user_surname, '') = ''
    OR COALESCE(sex, '') = ''
    OR COALESCE(password, '') = ''
    OR COALESCE(email, '') = ''
    OR COALESCE(university_name, '') = '';

  -- Drop temporary tables if they exist
  DROP TABLE IF EXISTS ExistingUniversities;
  DROP TABLE IF EXISTS ExistingUsers;

  -- Create temporary tables for existing data
  CREATE TEMP TABLE ExistingUniversities AS TABLE Universities;
  CREATE TEMP TABLE ExistingUsers AS TABLE Users;

  -- Insert new universities that do not exist already
  INSERT INTO Universities (university_name, address, city, country)
  SELECT t.university_name, t.address, t.city, t.country
  FROM TempUniversities t
  LEFT JOIN ExistingUniversities e
  ON t.university_name = e.university_name AND t.address = e.address AND t.city = e.city AND t.country = e.country
  WHERE e.university_name IS NULL;

  -- Insert new users that do not exist already
  INSERT INTO Users (university_id, user_name, user_surname, sex, password, email)
  SELECT e.university_id, t.user_name, t.user_surname, t.sex, crypt(t.password, gen_salt('bf')), t.email
  FROM TempUsers t
  JOIN Universities e ON t.university_name = e.university_name
  LEFT JOIN ExistingUsers u
  ON t.email = u.email AND t.user_name = u.user_name AND t.user_surname = u.user_surname
  WHERE u.email IS NULL;

  -- Logging successful completion
  RAISE NOTICE 'Data backup and loading completed successfully';
END;
$$ LANGUAGE plpgsql;

-- Call the function with parameters
SELECT backup_and_load_data_from_csv(
  'C:/esde/db/course_project/example_2.csv',
  'C:/esde/db/course_project/example_1.csv',
  'C:/esde/db/course_project/backup_csv'
);
