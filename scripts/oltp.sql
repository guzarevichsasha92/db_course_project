-- Drop dependent functions if they exist
DROP PROCEDURE IF EXISTS EnrollUserInCourse(INT, INT);
DROP PROCEDURE IF EXISTS SubmitAssignment(INT, INT, TEXT);

-- Drop existing tables if they exist
DROP TABLE IF EXISTS Grades;
DROP TABLE IF EXISTS Submissions;
DROP TABLE IF EXISTS Assignments;
DROP TABLE IF EXISTS Posts;
DROP TABLE IF EXISTS Enrollments;
DROP TABLE IF EXISTS Courses;

DROP TABLE IF EXISTS UserRoles;
DROP TABLE IF EXISTS Roles;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Universities;

-- Revoke privileges and drop existing roles if they exist
DO $$
DECLARE
    r RECORD;
BEGIN
    FOR r IN (SELECT rolname FROM pg_roles WHERE rolname IN ('admin_role', 'professor_role', 'student_role')) LOOP
        EXECUTE 'REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM ' || r.rolname;
        EXECUTE 'REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM ' || r.rolname;
        EXECUTE 'REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM ' || r.rolname;
        EXECUTE 'DROP ROLE IF EXISTS ' || r.rolname;
    END LOOP;
END
$$;

-- Create PostgreSQL roles
CREATE ROLE admin_role;
CREATE ROLE professor_role;
CREATE ROLE student_role;

-- Create Universities table
CREATE TABLE Universities (
    university_id SERIAL PRIMARY KEY,
    university_name VARCHAR(100) NOT NULL,
    address TEXT,
    city VARCHAR(50),
    country VARCHAR(50)
);

-- Create Users table with password hashing
CREATE TABLE Users (
    user_id SERIAL PRIMARY KEY,
    university_id INTEGER,
    user_name VARCHAR(50) NOT NULL,
    user_surname VARCHAR(50) NOT NULL,
    sex TEXT CHECK (sex IN ('male', 'female')),
    password TEXT NOT NULL, -- Passwords will be stored as hashed values
    email VARCHAR(100) NOT NULL UNIQUE,
    FOREIGN KEY (university_id) REFERENCES Universities(university_id) ON DELETE SET NULL
);

-- Create Roles table
CREATE TABLE Roles (
    role_id SERIAL PRIMARY KEY,
    role_name VARCHAR(50) NOT NULL
);

-- Insert default roles
INSERT INTO Roles (role_name) VALUES
('admin_role'),
('professor_role'),
('student_role');

-- Create UserRoles table
CREATE TABLE UserRoles (
    user_roles_id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES Roles(role_id) ON DELETE CASCADE,
    UNIQUE (user_id, role_id)
);

-- Create Courses table
CREATE TABLE Courses (
    course_id SERIAL PRIMARY KEY,
    university_id INTEGER NOT NULL,
    creator_id INTEGER NOT NULL,
    professor_id INTEGER NOT NULL,
    course_name VARCHAR(100) NOT NULL,
    description TEXT,
    FOREIGN KEY (university_id) REFERENCES Universities(university_id) ON DELETE CASCADE,
    FOREIGN KEY (creator_id) REFERENCES Users(user_id) ON DELETE CASCADE,
    FOREIGN KEY (professor_id) REFERENCES Users(user_id) ON DELETE CASCADE
);

-- Create Enrollments table
CREATE TABLE Enrollments (
    enrollment_id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    course_id INTEGER NOT NULL,
    enrollment_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE,
    FOREIGN KEY (course_id) REFERENCES Courses(course_id) ON DELETE CASCADE,
    UNIQUE (user_id, course_id)
);

-- Create Assignments table
CREATE TABLE Assignments (
    assignment_id SERIAL PRIMARY KEY,
    course_id INTEGER NOT NULL,
    assignment_title VARCHAR(100) NOT NULL,
    assignment_description TEXT,
    due_date TIMESTAMP,
    FOREIGN KEY (course_id) REFERENCES Courses(course_id) ON DELETE CASCADE
);

-- Create Submissions table
CREATE TABLE Submissions (
    submission_id SERIAL PRIMARY KEY,
    assignment_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    submission_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    submission_description TEXT,
    FOREIGN KEY (assignment_id) REFERENCES Assignments(assignment_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE,
    UNIQUE (assignment_id, user_id)
);

-- Create Grades table
CREATE TABLE Grades (
    grade_id SERIAL PRIMARY KEY,
    submission_id INTEGER NOT NULL,
    grade DECIMAL(5, 2),
    feedback TEXT,
    graded_by INTEGER NOT NULL,
    FOREIGN KEY (submission_id) REFERENCES Submissions(submission_id) ON DELETE CASCADE,
    FOREIGN KEY (graded_by) REFERENCES Users(user_id) ON DELETE CASCADE
);

-- Create Posts table
CREATE TABLE Posts (
    post_id SERIAL PRIMARY KEY,
    course_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    content TEXT,
    post_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (course_id) REFERENCES Courses(course_id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE CASCADE
);

-- Create indexes on frequently queried columns
CREATE INDEX idx_user_roles_user_id ON UserRoles(user_id);
CREATE INDEX idx_user_roles_role_id ON UserRoles(role_id);
CREATE INDEX idx_enrollments_user_id ON Enrollments(user_id);
CREATE INDEX idx_enrollments_course_id ON Enrollments(course_id);
CREATE INDEX idx_assignments_course_id ON Assignments(course_id);
CREATE INDEX idx_submissions_assignment_id ON Submissions(assignment_id);
CREATE INDEX idx_submissions_user_id ON Submissions(user_id);
CREATE INDEX idx_grades_submission_id ON Grades(submission_id);
CREATE INDEX idx_grades_graded_by ON Grades(graded_by);
CREATE INDEX idx_posts_course_id ON Posts(course_id);
CREATE INDEX idx_posts_user_id ON Posts(user_id);

-- Grant privileges to roles
GRANT ALL PRIVILEGES ON TABLE Users, Roles, UserRoles, Courses, Enrollments, Assignments, Submissions, Grades, Posts, Universities TO admin_role;
GRANT SELECT, INSERT, UPDATE ON TABLE Users, Courses, Assignments, Submissions, Grades, Posts TO professor_role;
GRANT SELECT, INSERT, UPDATE ON TABLE Submissions, Posts TO student_role;


-- Create procedure to enroll user in a course with error handling
CREATE OR REPLACE PROCEDURE EnrollUserInCourse(p_user_id INT, p_course_id INT)
LANGUAGE plpgsql AS $$
BEGIN
    BEGIN
        INSERT INTO Enrollments (user_id, course_id, enrollment_date)
        VALUES (p_user_id, p_course_id, NOW());
    EXCEPTION
        WHEN unique_violation THEN
            RAISE NOTICE 'User % is already enrolled in course %', p_user_id, p_course_id;
        WHEN others THEN
            RAISE EXCEPTION 'Error enrolling user % in course %: %', p_user_id, p_course_id, SQLERRM;
    END;
END;
$$;

-- Create procedure to submit assignment with error handling
CREATE OR REPLACE PROCEDURE SubmitAssignment(p_assignment_id INT, p_user_id INT, p_description TEXT)
LANGUAGE plpgsql AS $$
BEGIN
    BEGIN
        INSERT INTO Submissions (assignment_id, user_id, submission_date, submission_description)
        VALUES (p_assignment_id, p_user_id, NOW(), p_description);
    EXCEPTION
        WHEN others THEN
            RAISE EXCEPTION 'Error submitting assignment % by user %: %', p_assignment_id, p_user_id, SQLERRM;
    END;
END;
$$;

-- Grant execute privileges to roles for procedures
GRANT EXECUTE ON PROCEDURE EnrollUserInCourse, SubmitAssignment TO professor_role;
GRANT EXECUTE ON PROCEDURE SubmitAssignment TO student_role;