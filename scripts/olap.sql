-- Drop existing OLAP tables if they exist
DROP TABLE IF EXISTS Fact_Submissions CASCADE;
DROP TABLE IF EXISTS Fact_Enrollments CASCADE;
DROP TABLE IF EXISTS Dim_Course CASCADE;
DROP TABLE IF EXISTS Dim_Student CASCADE;
DROP TABLE IF EXISTS Dim_Assignment CASCADE;
DROP TABLE IF EXISTS Dim_Date CASCADE;
DROP TABLE IF EXISTS Dim_Student_SCD CASCADE;
DROP TABLE IF EXISTS Dim_University CASCADE;
DROP TABLE IF EXISTS Dim_Professor CASCADE;
DROP TABLE IF EXISTS Dim_Grade CASCADE;

-- Create Dimension Tables
CREATE TABLE Dim_University (
    university_id SERIAL PRIMARY KEY,
    university_name VARCHAR(100) NOT NULL,
    address TEXT,
    city VARCHAR(50),
    country VARCHAR(50)
);

CREATE TABLE Dim_Professor (
    professor_id SERIAL PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    university_id INT NOT NULL,
    FOREIGN KEY (university_id) REFERENCES Dim_University(university_id)
);

CREATE TABLE Dim_Course (
    course_id SERIAL PRIMARY KEY,
    course_name VARCHAR(100) NOT NULL,
    university_id INT NOT NULL,
    professor_id INT NOT NULL,
    description TEXT,
    FOREIGN KEY (university_id) REFERENCES Dim_University(university_id),
    FOREIGN KEY (professor_id) REFERENCES Dim_Professor(professor_id)
);

CREATE TABLE Dim_Student (
    student_id SERIAL PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL,
    sex VARCHAR(10) NOT NULL,
    email VARCHAR(100) NOT NULL,
    university_id INT NOT NULL,
    FOREIGN KEY (university_id) REFERENCES Dim_University(university_id)
);

CREATE TABLE Dim_Assignment (
    assignment_id SERIAL PRIMARY KEY,
    course_id INT NOT NULL,
    assignment_title VARCHAR(100) NOT NULL,
    due_date DATE NOT NULL,
    FOREIGN KEY (course_id) REFERENCES Dim_Course(course_id)
);

CREATE TABLE Dim_Date (
    date_key SERIAL PRIMARY KEY,
    date DATE NOT NULL,
    year INT NOT NULL,
    month INT NOT NULL,
    day INT NOT NULL,
    quarter INT NOT NULL
);

CREATE TABLE Dim_Grade (
    grade_id SERIAL PRIMARY KEY,
    grade_name VARCHAR(10) NOT NULL,
    grade_point DECIMAL(3, 2) NOT NULL
);

-- SCD Type 2 for Students
CREATE TABLE Dim_Student_SCD (
    student_sk SERIAL PRIMARY KEY,
    student_id INT NOT NULL,
    full_name VARCHAR(100) NOT NULL,
    sex VARCHAR(10) NOT NULL,
    email VARCHAR(100) NOT NULL,
    university_id INT NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL DEFAULT '9999-12-31',
    is_current BOOLEAN DEFAULT TRUE,
    UNIQUE (student_id, start_date),
    FOREIGN KEY (university_id) REFERENCES Dim_University(university_id)
);

-- Create Fact Tables
CREATE TABLE Fact_Submissions (
    submission_id SERIAL PRIMARY KEY,
    assignment_id INT NOT NULL,
    student_id INT NOT NULL,
    submission_date DATE NOT NULL,
    grade_id INT NOT NULL,
    graded_by INT NOT NULL,
    FOREIGN KEY (assignment_id) REFERENCES Dim_Assignment(assignment_id),
    FOREIGN KEY (student_id) REFERENCES Dim_Student_SCD(student_sk),
    FOREIGN KEY (grade_id) REFERENCES Dim_Grade(grade_id),
    FOREIGN KEY (graded_by) REFERENCES Dim_Student_SCD(student_sk)
);

CREATE TABLE Fact_Enrollments (
    enrollment_id SERIAL PRIMARY KEY,
    student_id INT NOT NULL,
    course_id INT NOT NULL,
    enrollment_date DATE NOT NULL,
    FOREIGN KEY (student_id) REFERENCES Dim_Student_SCD(student_sk),
    FOREIGN KEY (course_id) REFERENCES Dim_Course(course_id)
);

-- SCD2 Trigger for Dim_Student
CREATE OR REPLACE FUNCTION Dim_Student_SCD_update_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF (OLD.full_name IS DISTINCT FROM NEW.full_name OR
        OLD.sex IS DISTINCT FROM NEW.sex OR
        OLD.email IS DISTINCT FROM NEW.email OR
        OLD.university_id IS DISTINCT FROM NEW.university_id) AND OLD.is_current THEN

        UPDATE Dim_Student_SCD
        SET end_date = CURRENT_DATE - INTERVAL '1 day',
            is_current = FALSE
        WHERE student_id = OLD.student_id AND is_current = TRUE;

        INSERT INTO Dim_Student_SCD (
            student_id, full_name, sex, email, university_id, start_date, end_date, is_current
        )
        VALUES (
            OLD.student_id, NEW.full_name, NEW.sex, NEW.email, NEW.university_id,
            CURRENT_DATE, '9999-12-31', TRUE
        );
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- Create the trigger
CREATE TRIGGER Dim_Student_SCD_update
AFTER UPDATE ON Dim_Student
FOR EACH ROW
EXECUTE FUNCTION Dim_Student_SCD_update_trigger();