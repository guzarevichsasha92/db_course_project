-- Step 1: Create Staging Tables
CREATE SCHEMA IF NOT EXISTS staging;

-- Create staging tables with indexes
CREATE TABLE IF NOT EXISTS staging.Dim_University (
    university_id INT PRIMARY KEY,
    university_name VARCHAR(100),
    address TEXT,
    city VARCHAR(50),
    country VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS staging.Dim_Professor (
    professor_id INT PRIMARY KEY,
    full_name VARCHAR(100),
    email VARCHAR(100),
    university_id INT
);

CREATE TABLE IF NOT EXISTS staging.Dim_Course (
    course_id INT PRIMARY KEY,
    course_name VARCHAR(100),
    university_id INT,
    professor_id INT,
    description TEXT
);

CREATE TABLE IF NOT EXISTS staging.Dim_Student (
    student_id INT PRIMARY KEY,
    student_name VARCHAR(50),
    student_surname VARCHAR(50),
    sex VARCHAR(10),
    email VARCHAR(100),
    university_id INT
);

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_indexes WHERE schemaname = 'staging' AND indexname = 'idx_student_email') THEN
        CREATE INDEX idx_student_email ON staging.Dim_Student (email);
    END IF;
END $$;

CREATE TABLE IF NOT EXISTS staging.Dim_Assignment (
    assignment_id INT PRIMARY KEY,
    course_id INT,
    assignment_title VARCHAR(100),
    due_date DATE
);

CREATE TABLE IF NOT EXISTS staging.Dim_Grade (
    grade_id INT PRIMARY KEY,
    grade_name VARCHAR(10),
    grade_point DECIMAL(3, 2)
);

CREATE TABLE IF NOT EXISTS staging.Fact_Enrollments (
    enrollment_id INT PRIMARY KEY,
    student_id INT,
    course_id INT,
    enrollment_date DATE
);

CREATE TABLE IF NOT EXISTS staging.Fact_Submissions (
    submission_id INT PRIMARY KEY,
    assignment_id INT,
    student_id INT,
    submission_date DATE,
    grade_id INT,
    graded_by INT
);

-- Add additional indexes for performance
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_indexes WHERE schemaname = 'staging' AND indexname = 'idx_course_university') THEN
        CREATE INDEX idx_course_university ON staging.Dim_Course (university_id);
    END IF;
    IF NOT EXISTS (SELECT 1 FROM pg_indexes WHERE schemaname = 'staging' AND indexname = 'idx_enrollment_student') THEN
        CREATE INDEX idx_enrollment_student ON staging.Fact_Enrollments (student_id);
    END IF;
    IF NOT EXISTS (SELECT 1 FROM pg_indexes WHERE schemaname = 'staging' AND indexname = 'idx_enrollment_course') THEN
        CREATE INDEX idx_enrollment_course ON staging.Fact_Enrollments (course_id);
    END IF;
    IF NOT EXISTS (SELECT 1 FROM pg_indexes WHERE schemaname = 'staging' AND indexname = 'idx_submission_student') THEN
        CREATE INDEX idx_submission_student ON staging.Fact_Submissions (student_id);
    END IF;
    IF NOT EXISTS (SELECT 1 FROM pg_indexes WHERE schemaname = 'staging' AND indexname = 'idx_submission_assignment') THEN
        CREATE INDEX idx_submission_assignment ON staging.Fact_Submissions (assignment_id);
    END IF;
END $$;

-- Step 2: Extraction
DO $$
BEGIN
    -- Extract Universities
    INSERT INTO staging.Dim_University (university_id, university_name, address, city, country)
    SELECT u.university_id, u.university_name, u.address, u.city, u.country
    FROM Universities u
    LEFT JOIN Dim_University du ON u.university_id = du.university_id
    WHERE du.university_id IS NULL;

    -- Extract Professors
    INSERT INTO staging.Dim_Professor (professor_id, full_name, email, university_id)
    SELECT p.user_id AS professor_id, p.user_name || ' ' || p.user_surname AS full_name, p.email, p.university_id
    FROM Users p
    JOIN UserRoles ur ON p.user_id = ur.user_id
    JOIN Roles r ON ur.role_id = r.role_id
    LEFT JOIN Dim_Professor dp ON p.user_id = dp.professor_id
    WHERE r.role_name = 'professor_role' AND dp.professor_id IS NULL;

    -- Extract Courses
    INSERT INTO staging.Dim_Course (course_id, course_name, university_id, professor_id, description)
    SELECT c.course_id, c.course_name, c.university_id, c.professor_id, c.description
    FROM Courses c
    LEFT JOIN Dim_Course dc ON c.course_id = dc.course_id
    WHERE dc.course_id IS NULL;

    -- Extract Students
    INSERT INTO staging.Dim_Student (student_id, student_name, student_surname, sex, email, university_id)
    SELECT u.user_id, u.user_name, u.user_surname, u.sex, u.email, u.university_id
    FROM Users u
    LEFT JOIN Dim_Student ds ON u.user_id = ds.student_id
    WHERE ds.student_id IS NULL;

    -- Extract Assignments
    INSERT INTO staging.Dim_Assignment (assignment_id, course_id, assignment_title, due_date)
    SELECT a.assignment_id, a.course_id, a.assignment_title, a.due_date
    FROM Assignments a
    LEFT JOIN Dim_Assignment da ON a.assignment_id = da.assignment_id
    WHERE da.assignment_id IS NULL;

    -- Extract Grades
    INSERT INTO staging.Dim_Grade (grade_id, grade_name, grade_point)
    SELECT g.grade_id, 'Grade' || g.grade_id AS grade_name, g.grade AS grade_point
    FROM Grades g
    LEFT JOIN Dim_Grade dg ON g.grade_id = dg.grade_id
    WHERE dg.grade_id IS NULL;

    -- Extract Enrollments
    INSERT INTO staging.Fact_Enrollments (enrollment_id, student_id, course_id, enrollment_date)
    SELECT e.enrollment_id, e.user_id AS student_id, e.course_id, e.enrollment_date
    FROM Enrollments e
    LEFT JOIN Fact_Enrollments fe ON e.enrollment_id = fe.enrollment_id
    WHERE fe.enrollment_id IS NULL;

    -- Extract Submissions
    INSERT INTO staging.Fact_Submissions (submission_id, assignment_id, student_id, submission_date, grade_id, graded_by)
    SELECT s.submission_id, s.assignment_id, s.user_id AS student_id, s.submission_date, g.grade_id, g.graded_by
    FROM Submissions s
    LEFT JOIN Grades g ON s.submission_id = g.submission_id
    LEFT JOIN Fact_Submissions fs ON s.submission_id = fs.submission_id
    WHERE fs.submission_id IS NULL;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE 'Error during extraction: %', SQLERRM;
END $$;

-- Step 3: Validation and Transformation
DO $$
BEGIN
    -- Validate and Transform Universities
    DELETE FROM staging.Dim_University
    WHERE university_name IS NULL OR university_name = '';

    -- Validate and Transform Professors
    DELETE FROM staging.Dim_Professor
    WHERE email !~* '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$';

    UPDATE staging.Dim_Professor
    SET email = LOWER(email);

    -- Validate and Transform Students
    DELETE FROM staging.Dim_Student
    WHERE email !~* '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$';

    UPDATE staging.Dim_Student
    SET email = LOWER(email);

    -- Validate and Transform Courses
    DELETE FROM staging.Dim_Course
    WHERE course_name IS NULL OR course_name = '';

    -- Validate and Transform Assignments
    DELETE FROM staging.Dim_Assignment
    WHERE assignment_title IS NULL OR assignment_title = '';

    -- Validate and Transform Grades
    DELETE FROM staging.Dim_Grade
    WHERE grade_name IS NULL OR grade_name = '';

    -- Validate and Transform Enrollments
    UPDATE staging.Fact_Enrollments
    SET enrollment_date = CURRENT_DATE
    WHERE enrollment_date IS NULL;

    -- Validate and Transform Submissions
    UPDATE staging.Fact_Submissions
    SET grade_id = 0
    WHERE grade_id IS NULL;

    UPDATE staging.Fact_Submissions
    SET graded_by = 0
    WHERE graded_by IS NULL;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE 'Error during validation and transformation: %', SQLERRM;
END $$;

-- Step 4: Load Data to OLAP
DO $$
BEGIN
    -- Load Universities
    INSERT INTO Dim_University (university_id, university_name, address, city, country)
    SELECT university_id, university_name, address, city, country
    FROM staging.Dim_University
    ON CONFLICT (university_id) DO NOTHING;

    -- Load Professors
    INSERT INTO Dim_Professor (professor_id, full_name, email, university_id)
    SELECT professor_id, full_name, email, university_id
    FROM staging.Dim_Professor
    ON CONFLICT (professor_id) DO NOTHING;

    -- Load Students
    INSERT INTO Dim_Student (student_id, full_name, sex, email, university_id)
    SELECT student_id, student_name || ' ' || student_surname AS full_name, sex, email, university_id
    FROM staging.Dim_Student
    ON CONFLICT (student_id) DO NOTHING;

    -- Load Courses
    INSERT INTO Dim_Course (course_id, course_name, university_id, professor_id, description)
    SELECT course_id, course_name, university_id, professor_id, description
    FROM staging.Dim_Course
    ON CONFLICT (course_id) DO NOTHING;

    -- Load Assignments
    INSERT INTO Dim_Assignment (assignment_id, course_id, assignment_title, due_date)
    SELECT assignment_id, course_id, assignment_title, due_date
    FROM staging.Dim_Assignment
    ON CONFLICT (assignment_id) DO NOTHING;

    -- Load Grades
    INSERT INTO Dim_Grade (grade_id, grade_name, grade_point)
    SELECT grade_id, grade_name, grade_point
    FROM staging.Dim_Grade
    ON CONFLICT (grade_id) DO NOTHING;

    -- Load Enrollments
    INSERT INTO Fact_Enrollments (enrollment_id, student_id, course_id, enrollment_date)
    SELECT enrollment_id, student_id, course_id, enrollment_date
    FROM staging.Fact_Enrollments
    ON CONFLICT (enrollment_id) DO NOTHING;

    -- Load Submissions
    INSERT INTO Fact_Submissions (submission_id, assignment_id, student_id, submission_date, grade_id, graded_by)
    SELECT submission_id, assignment_id, student_id, submission_date, grade_id, graded_by
    FROM staging.Fact_Submissions
    ON CONFLICT (submission_id) DO NOTHING;

    -- Truncate staging tables
    TRUNCATE TABLE staging.Dim_University;
    TRUNCATE TABLE staging.Dim_Professor;
    TRUNCATE TABLE staging.Dim_Student;
    TRUNCATE TABLE staging.Dim_Course;
    TRUNCATE TABLE staging.Dim_Assignment;
    TRUNCATE TABLE staging.Dim_Grade;
    TRUNCATE TABLE staging.Fact_Enrollments;
    TRUNCATE TABLE staging.Fact_Submissions;
EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE 'Error during loading: %', SQLERRM;
        ROLLBACK;
END $$;
